//2. let //var //const starts.

// var a=25
// var b=35
// console.log(a+b)

// if(true){
//     var msg="var welcome to js learnig"
// }
// console.log(msg)//acting as global variable.
// if(true){
//     let ing="let welcome to js learnig"
//     console.log(ing)//acting as local variable in a scope.
// }
// if(true){
//     const ing="const welcome to js learnig"
//     console.log(ing) //so const also works like let as local variable in a scope only
// }


// var a=25
// console.log(a)

// var a =45;
// console.log(a)

// let a=25
// console.log(a)
// let a=45
// console.log(a)  //so it leads to syntax error so it doesnt allows to redeclare the values again.

// const a =25
// console.log(a)
// const a=45 //it also same as like let it doesnt allows to redeclare the values again.

// let //var //const ends.
