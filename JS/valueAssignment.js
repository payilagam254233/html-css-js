
//3.vlaue assignment starts

/*var a =25
console.log(a)
a=55
console.log(a)*/ // it doesnt leads to any error.

/*let a=25
console.log(a)
a=65
console.log(a)*/ //this also works like var in this format.

/*const a=25
console.log(a)
a=65
console.log(a)*/  //so here we cant reassign a value in const.

/*const student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/  //so here we can reassign a value or key in an object at const value also.

/*let student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/ // in let also we can use like const. 

/*var student={Name:"yogi",age:21}
console.table(student)
console.table(student.Name)
student.Name="D.yogeshwaran"
console.table(student)*/  //but here if we ctry to do these the syntax error will occurs

// value asssignment ends
