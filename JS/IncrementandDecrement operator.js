
//Increment(++) or Decrement(--) operator starts.
/*let a=1
a++
console.log(a)

let b=5
b--
console.log(b)

//postfix increment and prefix increment
//postfix increment
let x=3
const y=x++
console.log("Y: ",y,"X: ",x)
//prefix increment
let n=5
const h=++n
console.log("N: ",n,"H: ",h)
//postfix decrement
let i=15
let j=i--
console.log("I: ",i,"J: ",j)
//prefix decrement
let d=5
const e=--d
console.log("E: ",e,"D ",d)*/
//Increment(++) or Decrement(--) operator ends.