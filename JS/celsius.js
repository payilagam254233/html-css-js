function convert(){
    let cel_value=document.getElementById("input").value
    const farhen_value = (cel_value*9/5)+32
    const result = document.getElementById("result")
    result.innerHTML=farhen_value.toFixed(3)+" degrees fahrenheit"
}
function remove(){
    const result = document.getElementById("result")
    result.innerHTML=""
}