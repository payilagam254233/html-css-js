
//Bitwise operator starts.
/* 
& --->Bitwise AND
| --->Bitwise OR
~ --->Bitwise NOT
^ --->Bitwise XOR
<< --->Left Shift
>> --->Right Shift
>>> --->Unsigned Shift 
*/
/*
//Bitwise AND &
let a=12
let b=24
console.log(a&b)//--->output 2
a&=b
console.log(a)

//Bitwise OR |
a=12
b=24
console.log(a|b)
a|=b
console.log(a)

//Bitwise NOT ~
a=12
console.log(~a)

//Bitwise XOR ^
a=12
b=6
console.log(a^b)
a^=a
console.log(a)

//Left shift <<
a=5
b=2
console.log(a<<b)
//Right shift >>
console.log(a>>b)
//Unsigned right shift  >>>
console.log(a>>>b)*/

//Bitwise operator ends.
